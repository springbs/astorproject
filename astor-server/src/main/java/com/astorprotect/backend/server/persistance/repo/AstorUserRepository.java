package com.astorprotect.backend.server.persistance.repo;

import com.astorprotect.backend.server.persistance.model.AstorUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AstorUserRepository extends JpaRepository<AstorUser, Long> {

    AstorUser findByIduser(Long id);

    AstorUser findByUsername(String username);

    AstorUser findByPhone(String phone);

    AstorUser findByEmail(String email);

}
