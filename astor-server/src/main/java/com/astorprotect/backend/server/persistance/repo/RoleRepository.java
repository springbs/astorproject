package com.astorprotect.backend.server.persistance.repo;

import com.astorprotect.backend.server.persistance.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<UserRole, Long> {

    UserRole findByAccountType(String accountType);

}
