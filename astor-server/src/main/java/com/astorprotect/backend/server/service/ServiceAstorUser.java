package com.astorprotect.backend.server.service;

import com.astorprotect.backend.server.persistance.model.AstorUser;
import com.astorprotect.backend.server.persistance.model.UserRole;
import com.astorprotect.backend.server.persistance.repo.AstorUserRepository;
import com.astorprotect.backend.server.persistance.repo.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceAstorUser {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AstorUserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AstorUser findById(Long id) {
        return userRepository.getOne(id);
    }

    public AstorUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public AstorUser findByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }

    public AstorUser findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public List<AstorUser> getAllUsers() {
        return userRepository.findAll();
    }

    public AstorUser save(AstorUser user) {
        return userRepository.save(user);
    }

    public AstorUser saveUser(AstorUser user) {
        String hashpwd = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(hashpwd);
        return userRepository.save(user);

    }

    public UserRole saveRole(UserRole role) {
        return roleRepository.save(role);
    }

    public void addRoleToUser(String username, String accountType) {
        UserRole role = roleRepository.findByAccountType(accountType);
        AstorUser user = userRepository.findByUsername(username);
        user.getRoles().add(role);
        userRepository.save(user);
    }

    public AstorUser findUserByUsernameOrEmailOrPhone(String name) {
        return null;
    }

    public boolean delectAdmin(Long id_admin) {
        userRepository.deleteById(id_admin);
        return true;
    }

    public List<AstorUser> findAllUsers() {
        return userRepository.findAll();
    }

}
