package com.astorprotect.backend.server.service;

//<editor-fold defaultstate="collapsed" desc="Imports">
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import org.springframework.stereotype.Service;
//</editor-fold>

@Service
public class EmailService {

    @Autowired
    JavaMailSender mailSender;

    public void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(to);
            mailMessage.setSubject(subject);
            mailMessage.setText(text);

            mailSender.send(mailMessage);

        } catch (MailException e) {
            e.printStackTrace();
        }
    }

    public void sendSimpleMessaeUsingTemplate(String to, String subject, SimpleMailMessage template, String... templateArgs) {
        String text = String.format(template.getText(), templateArgs);
        sendSimpleMessage(to, subject, text);
    }

    public void sendMessageWithAttachement(String to, String subject, String text, String pathToAttachement) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            //mettre 'true' au constructeur pour creer un mmessage Multipart
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);

            FileSystemResource fileSystemResource = new FileSystemResource(new File(pathToAttachement));
            helper.addAttachment("Invoice", fileSystemResource);

            mailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
