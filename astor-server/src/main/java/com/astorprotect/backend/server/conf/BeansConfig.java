/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.astorprotect.backend.server.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Ulrich
 */
@Configuration
public class BeansConfig {

    @Bean
    public BCryptPasswordEncoder getBcp() {
        return new BCryptPasswordEncoder();
    }

}
